<?php

    /**
     * WEB4PRO - Creating profitable online stores
     *
     *
     * @author    WEB4PRO <spoprigin@web4pro.com.ua>
     * @category  WEB4PRO
     * @package   Web4pro_Quickview
     * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
     * @license   http://www.web4pro.net/license.txt
     */
    class Web4pro_Quickview_Block_Scripts extends Mage_Core_Block_Template {

        protected $product;

        public function getProduct() {
            if(empty($this->product)) {
                $this->product = Mage::registry('product');
            }
            return $this->product;

        }

        /**
         * check, if product type is configurable
         * @return bool
         */
        public function isConfigurable() {
            return $this->getProduct()->isConfigurable();
        }

        public function isBundle() {
            return ($this->getProduct()->getTypeInstance() instanceof Mage_Bundle_Model_Product_Type);
        }
        /**
         * get product config
         * @return mixed
         */
        public function getJsonConfig() {
            return Mage::app()->getLayout()->getBlock('product.info.options.configurable')->getJsonConfig();
        }

        /**
         * hide OR
         * @return bool
         */
        public function hideOr() {
            return Mage::helper('web4pro_quickview')->hideOr();
        }

        /**
         * get ajax price config
         * @return mixed
         */
        public function getPriceConfig() {
            return Mage::app()->getLayout()->getBlock('product.info')->getJsonConfig();
        }

        /**
         * get bundle options
         * @return mixed
         */
        public function getBundleJsonConfig() {
            return Mage::app()->getLayout()->getBlock('product.info.bundle')->getJsonConfig();
        }
    }
<?php

    /**
     * WEB4PRO - Creating profitable online stores
     *
     *
     * @author    WEB4PRO <spoprigin@web4pro.com.ua>
     * @category  WEB4PRO
     * @package   Web4pro_Quickview
     * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
     * @license   http://www.web4pro.net/license.txt
     */
    class Web4pro_Quickview_Model_Observer {

        protected $currentAction;
        protected $allovedActions = array('catalogsearch_result_index', 'catalog_category_view');

        public function __construct() {
            $this->currentAction = Mage::app()->getFrontController()->getAction()->getFullActionName();
        }	
	

	public function isQuickView() {
		return $this->currentAction == 'web4pro_quickview_index_getFastView';
	}	
        /**
         * @dispatch core_block_abstract_to_html_after
         * @param Varien_Event_Observer $observer
         * @return $this
         */
        public function checkReviews(Varien_Event_Observer $observer) {
            $block = $observer->getBlock();
            if ($block instanceof Mage_Review_Block_Helper && $this->isQuickView()) {
                if(Mage::helper('web4pro_quickview')->hideReviews()){
                    $observer->getTransport()->setHtml('');
                }
            }
        }

        /**
         * @dispatch core_block_abstract_to_html_after
         * @param Varien_Event_Observer $observer
         * @return $this
         */
        public function checkPrice(Varien_Event_Observer $observer) {
            $block = $observer->getBlock();
            if ($block instanceof Mage_Catalog_Block_Product_Price && !($block instanceof Mage_Bundle_Block_Catalog_Product_View_Type_Bundle_Option) && $this->isQuickView()) {
                if(Mage::helper('web4pro_quickview')->hidePrice()) {
                    $observer->getTransport()->setHtml('');
                }
            }
        }

        /**
         * add popup link under price block
         * @param Varien_Event_Observer $observer
         */
        public function printLink(Varien_Event_Observer $observer) {
            $block = $observer->getBlock();
            if ($block instanceof Mage_Catalog_Block_Product_Price && $this->isAllowedAction()) {
                if(Mage::helper('web4pro_quickview')->isEnabled()) {
                    $currentHtml = $observer->getTransport()->getHtml();
                    $html = Mage::app()->getLayout()->createBlock('core/template')->setTemplate('web4pro/quickview/catalog/product/link.phtml')->setProduct($block->getProduct())->toHtml();
                    $newHtml = $currentHtml . $html;
                    $observer->getTransport()->setHtml($newHtml);
                }
            }
        }

        /**
         * delete uenc for redirection to referer url instead of popup url (uenc) after adding to compare
         * @param Varien_Event_Observer $observer
         */
        public function checkRedirect(Varien_Event_Observer $observer) {
            Mage::app()->getRequest()->setParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_URL_ENCODED, null);
        }

        /**
         * check allowed action
         * @return bool
         */
        public function isAllowedAction() {
            return in_array($this->currentAction, $this->allovedActions);
        }
    }

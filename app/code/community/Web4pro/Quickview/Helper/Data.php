<?php
/**
 * WEB4PRO - Creating profitable online stores
 *
 *
 * @author    WEB4PRO <spoprigin@web4pro.com.ua>
 * @category  WEB4PRO
 * @package   Web4pro_Quickview
 * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
 * @license   http://www.web4pro.net/license.txt
 */
class Web4pro_Quickview_Helper_Data extends Mage_Core_Helper_Abstract {

    const XML_PATH_MODULE_ENABLED = 'web4pro_quickview/general/enabled';
    const XML_PATH_SHOW_PICTURE = 'web4pro_quickview/general/show_picture';
    const XML_PATH_SHOW_PRICE = 'web4pro_quickview/general/show_price';
    const XML_PATH_SHOW_DESCRIPTION = 'web4pro_quickview/general/show_description';
    const XML_PATH_SHOW_CART = 'web4pro_quickview/general/show_cart';
    const XML_PATH_SHOW_ADD_TO = 'web4pro_quickview/general/show_addto';
    const XML_PATH_SHOW_REVIEWS = 'web4pro_quickview/general/show_reviews';
    //const XML_PATH_SHOW_PRODUCT_OPTIONS = 'web4pro_quickview/general/show_product_options';
    const POPUP_URL = '/quickview/index/getFastView';

    /**
    * config variables
    */
    protected $config = array(
                              self::XML_PATH_SHOW_PICTURE,
                              self::XML_PATH_SHOW_PRICE,
                              self::XML_PATH_SHOW_DESCRIPTION,
                              self::XML_PATH_SHOW_CART,
                              self::XML_PATH_SHOW_ADD_TO,
                              //self::XML_PATH_SHOW_PRODUCT_OPTIONS,
                              self::XML_PATH_SHOW_REVIEWS
                            );

    /**
    * is module enabled
    */
    public function isEnabled() {
        return Mage::getStoreConfigFlag(self::XML_PATH_MODULE_ENABLED) && $this->checkOptions();
    }

    public function getPopupUrl() {
        return self::POPUP_URL;
    }

    /**
    *
    */
    public function checkOptions() {
        $enabled = false;
        foreach($this->config as $option) {
            if(Mage::getStoreConfigFlag($option)) {
                $enabled = true;
                break;
            }
        }
        return $enabled;
    }

    /**
    * apply config
    */
    public function applyProductConfig(){
        foreach($this->config as $value){
            $configValue = Mage::getStoreConfigFlag($value);
            if($configValue) continue;
            switch ($value) {
                case self::XML_PATH_SHOW_PICTURE:
                    Mage::app()->getLayout()->getBlock('product.info')->unsetChild('media');
                    break;
                case self::XML_PATH_SHOW_PRICE:
                    Mage::app()->getLayout()->getBlock('product.info')->unsetChild('product_type_data');
                    Mage::app()->getLayout()->getBlock('product.info')->unsetChild('product_type_availability');
                    Mage::app()->getLayout()->getBlock('product.info')->unsetChild('bundle_prices');
                    break;
                case self::XML_PATH_SHOW_DESCRIPTION:
                    Mage::registry('product')->unsetData('short_description');
                    break;
                case self::XML_PATH_SHOW_CART:
                    Mage::app()->getLayout()->getBlock('product.info')->unsetChild('addtocart');
                    Mage::app()->getLayout()->getBlock('product.info.options.wrapper.bottom')
                                                                      ->unsetChild('product.info.addtocart');
                    break;
                case self::XML_PATH_SHOW_ADD_TO:
                    Mage::app()->getLayout()->getBlock('product.info')->unsetChild('addto');
                    Mage::app()->getLayout()->getBlock('product.info.options.wrapper.bottom')
                                            ->unsetChild('product.info.addto');
                    break;
            }
        }
    }

    /**
    * Hide OR after Add to Card Button
    */
    public function hideOr() {
        return !(int)Mage::getStoreConfigFlag(self::XML_PATH_SHOW_ADD_TO) || !(int)Mage::getStoreConfigFlag(self::XML_PATH_SHOW_CART) ||
                                  !((int)Mage::getStoreConfigFlag(self::XML_PATH_SHOW_ADD_TO) && (int)Mage::getStoreConfigFlag(self::XML_PATH_SHOW_CART));
    }

    /**
    * hide reviews
    */
    public function hideReviews() {
        return ! Mage::getStoreConfigFlag(self::XML_PATH_SHOW_REVIEWS);
    }

    /**
    * hide product price
    */
    public function hidePrice() {
        return ! Mage::getStoreConfigFlag(self::XML_PATH_SHOW_PRICE);
    }
}

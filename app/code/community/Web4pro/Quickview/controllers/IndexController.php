<?php

    /**
     * WEB4PRO - Creating profitable online stores
     *
     *
     * @author    WEB4PRO <spoprigin@web4pro.com.ua>
     * @category  WEB4PRO
     * @package   Web4pro_Queckview
     * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
     * @license   http://www.web4pro.net/license.txt
     */
    class Web4pro_Quickview_IndexController extends Mage_Core_Controller_Front_Action {

        /**
         * get product page for pop up
         */
        public function getFastViewAction() {
            $productId = (int)$this->getRequest()->getParam('id');
            $product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId())->load($productId);
            if (!$product) {
                throw new Mage_Core_Exception($this->__('Product is not loaded'));
            }
            Mage::register('current_product', $product);
            Mage::register('product', $product);
            Mage::getSingleton('catalog/session')->setLastViewedProductId($product->getId());
            $this->initProductLayout($product);
            Mage::helper('web4pro_quickview')->applyProductConfig();
            return $this->getResponse()->setBody($this->getLayout()->getBlock('content')->toHtml());
        }

        /**
         * product layout initialization
         * @param $product
         * @return $this
         */
        public function initProductLayout($product) {
            $update = $this->getLayout()->getUpdate();
            $update->addHandle('default');
            $this->addActionLayoutHandles();
            $update->addHandle('PRODUCT_TYPE_' . $product->getTypeId());
            $update->addHandle('PRODUCT_' . $product->getId());
            $this->loadLayoutUpdates();
            $this->generateLayoutXml()->generateLayoutBlocks();
            return $this;
        }
    }
